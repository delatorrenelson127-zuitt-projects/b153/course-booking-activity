
fetch("http://localhost:4000/courses")
.then(res => res.json())
.then(data => {
    data.forEach(a=>console.log(a))
    let header = document.querySelector("header")
    header.innerHTML = `<p>${data[2].name}</p>`    
})

// document refers to the HTML connected to our JS file. One of the methods that can be used here is querySelector(), which allows us to 'get' a specific HTML element (only at a time)
